/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author pasinee
 */
public class Lab3 {

     public static int add(int num1, int num2) {
        return num1+num2;
    }

    private static boolean checkRows(String[][] table, String currentplayer) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0].equals(currentplayer) && table[row][1].equals(currentplayer) && table[row][2].equals(currentplayer)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkColumns(String[][] table, String currentplayer) {
        for (int col = 0; col < 3; col++) {
            if (table[0][col].equals(currentplayer) && table[1][col].equals(currentplayer) && table[2][col].equals(currentplayer)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkDiagonals(String[][] table, String currentplayer) {
        if (table[0][0].equals(currentplayer) && table[1][1].equals(currentplayer) && table[2][2].equals(currentplayer)) {
            return true;
        }
        if (table[0][2].equals(currentplayer) && table[1][1].equals(currentplayer) && table[2][0].equals(currentplayer)) {
            return true;
        }
        return false;
    }
    
    public static boolean checkDraw(String[][] table) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col].equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /// Check Win
    public static boolean checkWin(String[][] table, String currentplayer) {
        return checkRows(table, currentplayer) || checkColumns(table, currentplayer) || checkDiagonals(table, currentplayer);
    }
    
    
    
    
    
    
    
    
  }
