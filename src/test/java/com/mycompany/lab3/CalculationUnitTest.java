package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pasinee
 */
public class CalculationUnitTest {
    
    public CalculationUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testadd_1_2output_3() {
        int result = Lab3.add(1,2);
        assertEquals(3, result);
    }
    
    @Test    
    public void testadd_2_2output_4() {
        int result = Lab3.add(2 , 2);
        assertEquals(4, result);
    }
    
    @Test    
    public void testadd_9_17output_26() {
        int result = Lab3.add(9 , 17);
        assertEquals(26, result);
    }
}
