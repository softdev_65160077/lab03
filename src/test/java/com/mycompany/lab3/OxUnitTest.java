/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pasinee
 */
public class OxUnitTest {
    
    public OxUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWin_O_Verticel1_output_true(){
        String[][] table = {{"o","o","o"},{"-","-","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table,currentplayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Verticel2_output_true(){
        String[][] table = {{"-","-","-"},{"o","o","o"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table,currentplayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Verticel3_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"o","o","o"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table,currentplayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical4_output_true(){
        String[][] table = {{"o","-","-"},{"o","-","-"},{"o","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical5_output_true(){
        String[][] table = {{"-","o","-"},{"-","o","-"},{"-","o","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical6_output_true(){
        String[][] table = {{"-","-","o"},{"-","-","o"},{"-","-","o"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical7_output_true(){
        String[][] table = {{"o","-","-"},{"-","o","-"},{"-","-","o"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical8_output_true(){
        String[][] table = {{"-","-","o"},{"-","o","-"},{"o","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(true, result);
    }
    
    /////// Draw //////
    
    @Test
    public void testCheckWin_Draw_output_false(){
        String[][] table = {{"o","x","o"},{"o","o","x"},{"x","o","x"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertFalse(result, "The game should be a draw");
    }
    
    ///////
    
    @Test
    public void testCheckWin_O_Vertical1_output_false(){
        String[][] table = {{"o","-","-"},{"o","-","-o"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical2_output_false(){
        String[][] table = {{"-","o","-"},{"-","o","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical3_output_false(){
        String[][] table = {{"-","-","o"},{"-","-","o"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical4_output_false(){
        String[][] table = {{"o","o","-"},{"-","-","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical5_output_false(){
        String[][] table = {{"-","-","-"},{"o","o","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical6_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"o","o-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical7_output_false(){
        String[][] table = {{"o","-","-"},{"-","o","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWin_O_Vertical8_output_false(){
        String[][] table = {{"-","-","o"},{"-","o","-"},{"-","-","-"}};
        String currentplayer = "o";
        boolean result = Lab3.checkWin(table, currentplayer);
        assertEquals(false, result);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
